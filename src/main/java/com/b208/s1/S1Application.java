package com.b208.s1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;

@SpringBootApplication
@RestController

public class S1Application {

	public static void main(String[] args) {
		SpringApplication.run(S1Application.class, args);
	}

	@GetMapping("/hello")
	public String hello(){
		return "Hi from our Java Springboot App!";
	}

	@GetMapping("/hi")
	public String hi(
			@RequestParam(value = "name", defaultValue = "John") String nameParameter
		){
			//System.out.println(nameParameter);
			return "Hi! My name is " + nameParameter + "!";
	}

	@GetMapping("/favefood")
	public String myFavouriteFood(
			@RequestParam(value = "food", defaultValue = "cake") String faveFoodParam)
	{
		return "Hi! My favourite food is " + faveFoodParam + "!";
	}

	@GetMapping("/greeting/{name}")
	public String greeting(
			@PathVariable("name") String nameParam)
	{
		return "Hello " + nameParam;
	}


	/* ACTIVITY */

	ArrayList<String> enrollees = new ArrayList<String>();

	@GetMapping("/enroll")
	public String enroll(
			@RequestParam(value = "user") String userParam
	){
		boolean hasNoInput = true;
		String user = userParam;
		while(hasNoInput){
			if (userParam.isEmpty()){
				return "Input User";
			} else if (enrollees.contains(user)) {
				return "User Already Enrolled.";
			} else {
				hasNoInput = false;
				enrollees.add(userParam);
			}
		}
		return "Welcome, " + userParam + "!";
	}


	@GetMapping("/getEnrollees")
	public ArrayList<String> getEnrollees(){
		return enrollees;
	}


	@GetMapping("/courses/{id}")
	public String getCourses(@PathVariable("id") String courseParam){

		HashMap<String,String> courseSched = new HashMap<>();

		courseSched.put("java101","Name: Java 101, Schedule: MWF 8:00AM - 11:00AM, Price PHP 3000");
		courseSched.put("sql101","Name: SQL 101, Schedule: TTh 8:00AM - 11:00AM, Price PHP 2000");
		courseSched.put("jsoop101","Name: JS OOP 101, Schedule: MWF 1:00PM - 5:00PM, Price PHP 2950");

		switch (courseParam){
			case "java101":
				return courseSched.get("java101");
			case "sql101":
				return courseSched.get("sql101");
			case "jsoop101":
				return courseSched.get("jsoop101");
			default:
				return "Course cannot be found.";
		}
	}

	/* END OF ACTIVITY */
}




/*
	Annotations
		- @Springbootapplication will tell springboot that this application will function as an endpoint handling web requests.
		- (the yellow fonts) in Java Springboot marks classes for their intended functionalities.
		- Springboot upon starting scans for classes and assign behaviours based on their annotation.
		- @GetMapping will map a route or an endpoint to access or run our method. When http://localhost:8080/hello is accessed from a client, we will be able to run the hi() method.

	2 ways of passing data through the URL by using Java Springboot

		@RequestParam
			- Query String using Request Params
			- directly passing data into the url and getting the data
			- Pass data through the url using our string query
				ex. http://localhost:8080/hi?name=Joe
			- in this example, we retrieve the value of the filed "name" from our URL
			- defaultValue is the fallback value when the query string or request param is empty.
			- if you change it to value = "email" then the url input should look like this: http://localhost:8080/hi?email=martha
			- without default value (@RequestParam(value = "name") String name) and with this url (http://localhost:8080/hi?name=) it will only return an empty value but will not have any error
			- adding space in url will look like http://localhost:8080/favefood?food=sinigang%20with%20cake

		@PathVariable
			- much more similar to Express JS req.params
			- (@PathVariable("name") String nameParam) with URL http://localhost:8080/greeting/Martha
			- the variable "{name}" in @GetMapping("/greeting/{name}") should match the one inside the @PathVariable("name") for it to read the name
*/